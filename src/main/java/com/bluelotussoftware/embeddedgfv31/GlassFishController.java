/*
 *  Copyright 2011 Blue Lotus Software, LLC.
 *  Copyright 2011 John Yeary <jyeary@bluelotussoftware.com>.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
/*
 * $Id:$
 */
package com.bluelotussoftware.embeddedgfv31;

import java.io.File;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.embeddable.GlassFish.Status;
import org.glassfish.embeddable.*;

/**
 * GlassFish Controller for starting an embedded GlassFish instance.
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
public class GlassFishController implements Runnable {

    private GlassFishProperties glassFishProperties;
    private GlassFish glassFish;
    private Deployer deployer;
    private String[] warFilePathElements;

    /**
     * Constructor. The constructor also initializes a shutdown hook
     *
     * @param port the port to assign to the &quot;http-listener&quot;
     * @throws GlassFishException if an exception occurs during instance
     * initialization.
     */
    public GlassFishController(int port) throws GlassFishException {
        glassFishProperties = new GlassFishProperties();
        glassFishProperties.setPort("http-listener", port);
        glassFish = GlassFishRuntime.bootstrap().newGlassFish(glassFishProperties);

        // add shutdown hook to stop server
        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                shutdown();
            }
        });
    }

    /**
     * Constructor. The constructor also initializes a shutdown hook
     *
     * @param warFilePathElements an array of path elements to war files to
     * deploy.
     * @param port the port to assign to the &quot;http-listener&quot;
     * @throws GlassFishException if an exception occurs during instance
     * initialization.
     */
    public GlassFishController(String[] warFilePathElements, int port) throws GlassFishException {
        this.warFilePathElements = warFilePathElements;
        glassFishProperties = new GlassFishProperties();
        glassFishProperties.setPort("http-listener", port);
        glassFish = GlassFishRuntime.bootstrap().newGlassFish(glassFishProperties);

        // add shutdown hook to stop server
        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                shutdown();
            }
        });
    }

    /**
     * Runnable implementation which automatically deploys any war files
     * supplied in the constructor.
     */
    @Override
    public void run() {
        try {
            glassFish.start();
            deployer = glassFish.getDeployer();

            for (String s : warFilePathElements) {
                String application = deployer.deploy(new File(s));
                System.out.println(application + " is deployed.");
            }

        } catch (GlassFishException ex) {
            Logger.getLogger(GlassFishController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This stops the GlassFish instance and un-deploys any war files.
     */
    public void stop() {
        try {
            Collection<String> applications = deployer.getDeployedApplications();

            for (String s : applications) {
                System.out.println("Undeploying " + s);
                deployer.undeploy(s);
            }

            glassFish.stop();
        } catch (GlassFishException ex) {
            Logger.getLogger(GlassFishController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This shuts down the GlassFish instance if running, and not disposed. This
     * method is called by the shutdown hook.
     */
    public void shutdown() {
        try {

            if (Status.STOPPED != glassFish.getStatus()) {
                stop();
            }

            if (Status.DISPOSED != glassFish.getStatus()) {
                glassFish.dispose();
            }

        } catch (GlassFishException ex) {
            Logger.getLogger(GlassFishController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Wrapper method which returns container deployer.
     *
     * @return an instance of the GlassFish
     * <code>Deployer</code> which is used to deploy war and ear files.
     * @throws GlassFishException if the deployer can not be retrieved.
     */
    public Deployer getDeployer() throws GlassFishException {
        return glassFish.getDeployer();
    }
}
