/*
 *  Copyright 2011 Blue Lotus Software, LLC.
 *  Copyright 2011 John Yeary <jyeary@bluelotussoftware.com>.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
/*
 * $Id:$
 */
package com.bluelotussoftware.embeddedgfv31;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.embeddable.GlassFishException;

/**
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
public class Main {

    public static void main(String[] args) {
        try {
            GlassFishController gfc = new GlassFishController(
                    new String[]{"example.war"}, 8080);
            Thread t = new Thread(gfc);
            t.start();
            System.out.println("\n\nPress CTRL + C to stop GlassFish Server.\n\n");
        } catch (GlassFishException e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
